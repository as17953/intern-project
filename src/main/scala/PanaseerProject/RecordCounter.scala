package PanaseerProject

import java.io.PrintWriter

import org.apache.spark.sql.DataFrame

object RecordCounter {

  // Pass as arguments:
  //  - Data frame to analyse
  //  - File path to where you want the number of records written to
  def countNoRecords(data: DataFrame, outputPath: String): Unit ={

    // Count how many records are in the data frame
    val noRecords = data.count()

    // Write the number of records to the output file
    new PrintWriter(outputPath){ write("Number of records: " + noRecords); close}


  }

}
