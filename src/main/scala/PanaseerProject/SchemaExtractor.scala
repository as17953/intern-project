package PanaseerProject

import java.io.PrintWriter

import org.apache.spark.sql.DataFrame

object SchemaExtractor {

  // Pass as arguments:
  //  - Data to analyse
  //  - File path to where you want the schema written to
  def schemaToFile(data: DataFrame, outputPath: String): Unit = {

    // Extract the schema (in tree format) from the DataFrame
    val schema = data.schema.treeString

    // Write the schema to the output file
    new PrintWriter(outputPath) { write(schema); close }

  }
}