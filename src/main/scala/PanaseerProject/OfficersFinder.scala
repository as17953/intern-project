package PanaseerProject

import java.io.PrintWriter

import org.apache.spark.sql.DataFrame


object OfficersFinder {

  // Pass as arguments:
  //  - Data to analyse
  //  - Output path to write distinct officers to
  def findAllOfficers(data: DataFrame, outputPath: String): Unit ={

    // Get the distinct elements of the CASEOFFICER field, collect them (to get array of rows instead of DataFrame)
    // and convert to a string with each officer on a new line, ready to write to file
    val distinctOfficers = data.select("CASEOFFICER").distinct()

    // Convert the rows from the data set to a readable format we can write to file
    val toFile = distinctOfficers.collect().mkString("\n")

    // Write the list of distinct officers to the output file
    new PrintWriter(outputPath){ write("Distinct officers: \n" + toFile); close}

  }

}
