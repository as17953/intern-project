Project for Summer internship at Panaseer.

The aim was to see how I adapted to using new technologies quickly. I had never used Scala or Apache Spark before, so it was interesting getting to grips with some of the exercises.

Exercises & Questions:

- Discover the schema of the input dataset and output it to a file.
- What is the total number of planning application records in the dataset? Feel free to output this to a file or standard output on the console.
- Identify the set of case officers (CASEOFFICER field) and output a unique list of these to a file.
- Who are the top N agents (AGENT field) submitting the most number of applications? Allow N to be configurable and output the list to a file.
- Count the occurrence of each word within the case text (CASETEXT field) across all planning application records. Output each word and the corresponding count to a file.
- Measure the average public consultation duration in days (i.e. the difference between PUBLICCONSULTATIONENDDATE and PUBLICCONSULTATIONSTARTDATE fields). Feel free to output this to a file or standard output on the console.
 