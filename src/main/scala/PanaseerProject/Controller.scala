
// Aidan Sweeney: "Final Round Coding Project" for Panaseer Summer internship"

package PanaseerProject

import org.apache.spark.sql.SparkSession

object Controller {

  // Program execution will start here and perform all 6 exercises on the data (as per email)
  // Output files will be saved to the project's resources directory
  def main(args: Array[String]): Unit = {

    // Check that the correct number of arguments were passed
    if (args.length == 2) {

      // Take the input file path from arguments
      val inputPath = args.apply(0)

      // Allow the number of top agents to be configurable
      val noAgents = args.apply(1).toInt

      // Initialise a spark session
      val spark: SparkSession = SparkSession.builder.master("local").getOrCreate

      // Read the input file to a DataFrame
      val dataFrame = spark.read.option("multiLine", true).json(inputPath)

      // Find the schema of the data
      SchemaExtractor.schemaToFile(dataFrame, "src/main/resources/schema")

      // Count how many records there are
      RecordCounter.countNoRecords(dataFrame, "src/main/resources/no-records")

      // List all officers
      OfficersFinder.findAllOfficers(dataFrame, "src/main/resources/distinct-officers")

      // Find top n agents in regards to how many applications an agent deals with
      AgentsFinder.findTopNAgents(dataFrame, "src/main/resources/top-n-agents", noAgents)

      // Count the occurrence of each word in the cases text
      WordCounter.countWordOccurrences(dataFrame, "src/main/resources/word-occurrences")

      // Measure average public consultation over all planning records
      AverageCalculator.getAverageConsultation(dataFrame, "src/main/resources/average-consultation")
    }
    else println("Only enter 2 arguments: [data location] [no. agents]")
  }

}
