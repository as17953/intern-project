package PanaseerProject

import java.io.PrintWriter

import org.apache.spark.sql.{DataFrame, Row}
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object AverageCalculator {


  // Pass as arguments:
  //  - Data to analyse
  //  - Output path to write to
  def getAverageConsultation(data: DataFrame, outputPath: String): Unit ={

    // Create a list of all time differences to add to
    val differences = scala.collection.mutable.MutableList[Int]()

    // Extract the start and end date columns
    val dates = data.select("PUBLICCONSULTATIONSTARTDATE", "PUBLICCONSULTATIONENDDATE").collect()

    // For each row, calculate the difference and add to list of differences
    dates.foreach(r => differences += calculateTimeDifference(r))

    // Calculate the average consultation time by averaging the values in the list
    val average = differences.sum / differences.length

    // Write the average to file
    new PrintWriter(outputPath) { write("Average consultation (in days) is: " + average); close }

  }

  // Calculate the difference between two given dates
  def calculateTimeDifference(row: Row): Int ={

    // Declare a time formatter (all dates are in the form DD/MM/YYYY)
    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")

    // If either date is in the wrong format
    if(row.apply(0) == "" || row.apply(1) == ""){

      // Return 0 for failed calculation
      return 0

    }

    // Otherwise, if the dates are in the right format, calculate and return the difference
    else{

      // Retrieve the start and end dates
      val start = row.apply(0).toString
      val end = row.apply(1).toString

      // Calculate the difference in time
      val difference = (LocalDate.parse(end, formatter).toEpochDay - LocalDate.parse(start, formatter).toEpochDay).toInt

      // Return the difference
      return difference


    }
  }
}
