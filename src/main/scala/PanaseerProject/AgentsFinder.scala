package PanaseerProject

import java.io.PrintWriter
import org.apache.spark.sql.DataFrame

import org.apache.spark.sql.functions._


object AgentsFinder {

  // Pass as arguments:
  //  - Data to analyse
  //  - Output path to write top n agents to
  def findTopNAgents(data: DataFrame, outputPath: String, noAgents: Int): Unit ={

    // Check that the number of top agents to return  is at most the total number of agents
    if(noAgents <= data.select("AGENT").count()) {

      // Get top n agent values in AGENT column, with their count, then sort in descending order of count
      val topAgents = data.groupBy("AGENT").count().sort(desc("count")).limit(noAgents)

      // Covert the rows from the DataFrame into a format we can write to file
      val toFile = topAgents.collect().mkString("\n")

      // Write the top n agents to the output file
      new PrintWriter(outputPath) {write("Top " + noAgents + " agents: \n" + toFile); close }

    }

    // Otherwise, signify that the number entered was too high
    else println("There aren't that many agents. Please try a number below " + data.select("AGENT").count())


  }

}
