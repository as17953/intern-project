package PanaseerProject

import java.io.PrintWriter

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions._


object WordCounter {

  // NOTE: There are some formatting errors since there are words mixed with symbols, etc. in some case texts

  // Pass as arguments:
  //  - Data to analyse
  //  - Output path to write word counts to
  def countWordOccurrences(data: DataFrame, outputPath: String): Unit = {

    // Get a data frame for all case texts
    val caseTexts = data.select("CASETEXT")

    // Split the contents of each case text record up into lists of words (split by a space) and give the column a new
    // alias 'asListWords' to make this clear
    val eachRecordAsListOfWords = caseTexts.select(split(caseTexts("CASETEXT"), " ").alias("asListWords"))

    // Extract words from each case text to its own row in a new data frame using explode, giving new alias 'asSingleWords'
    // to make this clear, like above
    val eachRecordAsSingleWords = eachRecordAsListOfWords.select(explode(eachRecordAsListOfWords("asListWords")).alias("asSingleWords"))

    // Count the occurrence of each word, so we have a data frame with values in the form (word, count)
    val eachWordWithCount = eachRecordAsSingleWords.groupBy("asSingleWords").count()

    // Convert these (word, count) pairings to a format we can write to file
    val toFile = eachWordWithCount.collect().mkString("\n")

    // Write the word counts to the output path
    new PrintWriter(outputPath) { write("Word counts (word, count): \n" + toFile); close }

  }

}